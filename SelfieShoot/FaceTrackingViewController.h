//
//  FaceTrackingViewController.h
//  SelfieShoot
//
//  Created by LWHo on 12/06/2017.
//  Copyright © 2017 CSIE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "RBLProtocol.h"
#import "BLE.h"

@class CIDetector;

@interface FaceTrackingViewController : UIViewController<UIGestureRecognizerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate,ProtocolDelegate>
{
    IBOutlet UIView *previewView;
    IBOutlet UISegmentedControl *camerasControl;
    AVCaptureVideoPreviewLayer *previewLayer;
    AVCaptureSession *session;
    AVCaptureVideoDataOutput *videoDataOutput;
    BOOL detectFaces;
    dispatch_queue_t videoDataOutputQueue;
    AVCaptureStillImageOutput *stillImageOutput;
    UIView *flashView;
    UIImageView *square;
    UIImageView *faceBox;
    BOOL isUsingFrontFacingCamera;
    CIDetector *faceDetector;
    CGFloat beginGestureScale;
    CGFloat effectiveScale;
    UILabel *faceCountLabel;
    IBOutlet UIToolbar *bottomToolbar;
    
    NSTimer *timerPicture;
    NSTimer *countDown;
    NSTimer *onoffTorch;
    CGPoint centerPoing;
    IBOutlet UIButton *Capture;
    IBOutlet UIImageView *showImg;
    
    AVCaptureConnection *videoConnection;
    NSInteger flash; //閃光燈初始
    
    //IBOutlet UIImageView *border;
    
}

@property (strong, nonatomic) BLE *ble;
@property (strong, nonatomic) RBLProtocol *protocol;
@property (strong, nonatomic) UIImageView *border;
@property (strong, nonatomic) UIPinchGestureRecognizer *handlePinch;   //使用縮放更改注視框大小
@property (strong, nonatomic) UIPanGestureRecognizer *panBorder;       //使用拖曳更改注視框位置
@property (strong, nonatomic) UIView *facePointView;
@property CGPoint currentLocation;
//@property (retain, nonatomic) IBOutlet UIImageView *myImg;

- (IBAction)takePicture:(id)sender;
- (IBAction)switchCameras:(id)sender;
//- (IBAction)handlePinchGesture:(UIGestureRecognizer *)sender;
- (IBAction)toggleFaceDetection:(id)sender;

@end

