//
//  FaceTrackingFaceTrackingViewController.m
//  SelfieShoot
//
//  Created by LWHo on 12/06/2017.
//  Copyright © 2017 CSIE. All rights reserved.
//


#import "FaceTrackingViewController.h"
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>

#pragma mark-

// used for KVO observation of the @"capturingStillImage" property to perform flash bulb animation
static const NSString *AVCaptureStillImageIsCapturingStillImageContext = @"AVCaptureStillImageIsCapturingStillImageContext";

static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

// utility used by newSquareOverlayedImageForFeatures for
static CGContextRef CreateCGBitmapContextForSize(CGSize size);
static CGContextRef CreateCGBitmapContextForSize(CGSize size)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    int             bitmapBytesPerRow;
    
    bitmapBytesPerRow = (size.width * 4);
    
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate (NULL,
                                     size.width,
                                     size.height,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedLast);
    CGContextSetAllowsAntialiasing(context, NO);
    CGColorSpaceRelease( colorSpace );
    return context;
}

#pragma mark-

@interface UIImage (RotationMethods)
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
@end

@implementation UIImage (RotationMethods)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    [rotatedViewBox release];
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

@end

#pragma mark-

@interface FaceTrackingViewController (InternalMethods)
- (void)setupAVCapture;
- (void)teardownAVCapture;
- (void)update:(id)sender;
- (void)update_Botton:(id)sender;

- (void)drawFaceBoxesForFeatures:(NSArray *)features forVideoBox:(CGRect)clap orientation:(UIDeviceOrientation)orientation;
@end

@implementation FaceTrackingViewController
@synthesize ble;
@synthesize protocol;


//選擇使用何種機構再編譯
#define BlendMicro  FALSE  //False -> Ver.1 ; TRUE -> Ver.2

#if BlendMicro
#   define maxlimit 110
#   define Servo360 106
#   define Servo362 110
#   define Servo358 100
#else
#   define maxlimit 125
#   define Servo360 90
#   define Servo362 93
#   define Servo358 87
#endif




#define STATE_INIT 0                                        //初始狀態
#define STATE_INSIDE_BORDER         STATE_INIT+1            //進入視訊框狀態
#define STATE_SMILE                 STATE_INSIDE_BORDER+1   //視訊框內偵測到微笑狀態
#define STATE_SHOTING               STATE_SMILE+1           //微笑中偵測到扎眼啟動拍照
#define STATE_SHOTING_EXIT_BORDER   STATE_SHOTING+1         //拍照中離開視訊框


int currentstate = STATE_INIT, faceCount = 0;

- (void)setupAVCapture
{
    NSError *error = nil;
    
    session = [AVCaptureSession new];
    //AVCaptureSession *session = [AVCaptureSession new];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        //[session setSessionPreset:AVCaptureSessionPreset640x480];
        //[session setSessionPreset:AVCaptureSessionPresetiFrame1280x720];
        [session setSessionPreset:AVCaptureSessionPresetHigh];
        //[session setSessionPreset:AVCaptureSessionPreset1920x1080];
    else
        [session setSessionPreset:AVCaptureSessionPresetPhoto];
    
    // Select a video device, make an input
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    require( error == nil, bail );
    
    isUsingFrontFacingCamera = NO;
    if ( [session canAddInput:deviceInput] )
        [session addInput:deviceInput];
    
    // Make a still image output
    stillImageOutput = [AVCaptureStillImageOutput new];
    [stillImageOutput addObserver:self forKeyPath:@"capturingStillImage" options:NSKeyValueObservingOptionNew context:AVCaptureStillImageIsCapturingStillImageContext];
    if ( [session canAddOutput:stillImageOutput] )
        [session addOutput:stillImageOutput];
    NSLog(@"addObserver ");
    
    // Make a video data output
    videoDataOutput = [AVCaptureVideoDataOutput new];
    
    // we want BGRA, both CoreGraphics and OpenGL work well with 'BGRA'
    NSDictionary *rgbOutputSettings = [NSDictionary dictionaryWithObject:
                                       [NSNumber numberWithInt:kCMPixelFormat_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    [videoDataOutput setVideoSettings:rgbOutputSettings];
    [videoDataOutput setAlwaysDiscardsLateVideoFrames:YES]; // discard if the data output queue is blocked (as we process the still image)
    
    // create a serial dispatch queue used for the sample buffer delegate as well as when a still image is captured
    // a serial dispatch queue must be used to guarantee that video frames will be delivered in order
    // see the header doc for setSampleBufferDelegate:queue: for more information
    videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
    [videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
    
    if ( [session canAddOutput:videoDataOutput] )
        [session addOutput:videoDataOutput];
    [[videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:NO];
    
    effectiveScale = 1.0;
    previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
    //previewLayer = [[AVCaptureConnection alloc] initWithSession:session];
    [previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspect];
    CALayer *rootLayer = [previewView layer];
    [rootLayer setMasksToBounds:YES];
    [previewLayer setFrame:[rootLayer bounds]];
    [rootLayer addSublayer:previewLayer];
    [session startRunning];
    
bail:
    [session release];
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Failed with error %d", (int)[error code]]
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        [self teardownAVCapture];
        
    }
}

// //clean up capture setup
//- (void)teardownAVCapture
//{
//    NSLog(@"removeObserver");
//    [videoDataOutput release];
//    if (videoDataOutputQueue)
//        dispatch_release(videoDataOutputQueue);
//    [stillImageOutput removeObserver:self forKeyPath:@"isCapturingStillImage"];
//    [stillImageOutput release];
//    [previewLayer removeFromSuperlayer];
//    [previewLayer release];
//
//}

// perform a flash bulb animation using KVO to monitor the value of the capturingStillImage property of the AVCaptureStillImageOutput class
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ( context == AVCaptureStillImageIsCapturingStillImageContext ) {
        BOOL isCapturingStillImage = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
        
        if ( isCapturingStillImage ) {
            // do flash bulb like animation
            flashView = [[UIView alloc] initWithFrame:[previewView frame]];
            [flashView setBackgroundColor:[UIColor whiteColor]];
            [flashView setAlpha:0.f];
            [[[self view] window] addSubview:flashView];
            
            [UIView animateWithDuration:.4f
                             animations:^{
                                 [flashView setAlpha:1.f];
                             }
             ];
        }
        else {
            [UIView animateWithDuration:.4f
                             animations:^{
                                 [flashView setAlpha:0.f];
                             }
                             completion:^(BOOL finished){
                                 [flashView removeFromSuperview];
                                 [flashView release];
                                 flashView = nil;
                             }
             ];
        }
    }
}

// utility routing used during image capture to set up capture orientation
- (AVCaptureVideoOrientation)avOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    AVCaptureVideoOrientation result = deviceOrientation;
    if ( deviceOrientation == UIDeviceOrientationLandscapeLeft )
        result = AVCaptureVideoOrientationLandscapeRight;
    else if ( deviceOrientation == UIDeviceOrientationLandscapeRight )
        result = AVCaptureVideoOrientationLandscapeLeft;
    return result;
}

// utility routine to create a new image with the red square overlay with appropriate orientation
// and return the new composited image which can be saved to the camera roll
- (CGImageRef)newSquareOverlayedImageForFeatures:(NSArray *)features
                                       inCGImage:(CGImageRef)backgroundImage
                                 withOrientation:(UIDeviceOrientation)orientation
                                     frontFacing:(BOOL)isFrontFacing
{
    CGImageRef returnImage = NULL;
    CGRect backgroundImageRect = CGRectMake(0., 0., CGImageGetWidth(backgroundImage), CGImageGetHeight(backgroundImage));
    CGContextRef bitmapContext = CreateCGBitmapContextForSize(backgroundImageRect.size);
    CGContextClearRect(bitmapContext, backgroundImageRect);
    CGContextDrawImage(bitmapContext, backgroundImageRect, backgroundImage);
    CGFloat rotationDegrees = 0.;
    
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            rotationDegrees = -90.;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            rotationDegrees = 90.;
            break;
        case UIDeviceOrientationLandscapeLeft:
            if (isFrontFacing) rotationDegrees = 180.;
            else rotationDegrees = 0.;
            break;
        case UIDeviceOrientationLandscapeRight:
            if (isFrontFacing) rotationDegrees = 0.;
            else rotationDegrees = 180.;
            break;
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
        default:
            break; // leave the layer in its last known orientation
    }
    UIImage *rotatedSquareImage = [square imageRotatedByDegrees:rotationDegrees];
    //UIImage *rotatedSquareImage = [faceBox imageRotatedByDegrees:rotationDegrees];
    
    // features found by the face detector
    for ( CIFaceFeature *ff in features ) {
        CGRect faceRect = [ff bounds];
        CGContextDrawImage(bitmapContext, faceRect, [rotatedSquareImage CGImage]);
    }
    returnImage = CGBitmapContextCreateImage(bitmapContext);
    CGContextRelease (bitmapContext);
    
    return returnImage;
}

// utility routine used after taking a still image to write the resulting image to the camera roll
- (BOOL)writeCGImageToCameraRoll:(CGImageRef)cgImage withMetadata:(NSDictionary *)metadata
{
    CFMutableDataRef destinationData = CFDataCreateMutable(kCFAllocatorDefault, 0);
    CGImageDestinationRef destination = CGImageDestinationCreateWithData(destinationData,
                                                                         CFSTR("public.jpeg"),
                                                                         1,
                                                                         NULL);
    BOOL success = (destination != NULL);
    require(success, bail);
    
    const float JPEGCompQuality = 0.0f; // JPEGHigherQuality
    CFMutableDictionaryRef optionsDict = NULL;
    CFNumberRef qualityNum = NULL;
    
    qualityNum = CFNumberCreate(0, kCFNumberFloatType, &JPEGCompQuality);
    if ( qualityNum ) {
        optionsDict = CFDictionaryCreateMutable(0, 0, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        if ( optionsDict )
            CFDictionarySetValue(optionsDict, kCGImageDestinationLossyCompressionQuality, qualityNum);
        CFRelease( qualityNum );
    }
    
    CGImageDestinationAddImage( destination, cgImage, optionsDict );
    success = CGImageDestinationFinalize( destination );
    
    if ( optionsDict )
        CFRelease(optionsDict);
    
    require(success, bail);
    
    CFRetain(destinationData);
    ALAssetsLibrary *library = [ALAssetsLibrary new];
    [library writeImageDataToSavedPhotosAlbum:(id)destinationData metadata:metadata completionBlock:^(NSURL *assetURL, NSError *error) {
        if (destinationData)
            CFRelease(destinationData);
    }];
    [library release];
    
    NSLog(@"Run writeCGImageToCameraRoll!!!!");
    
bail:
    if (destinationData)
        CFRelease(destinationData);
    if (destination)
        CFRelease(destination);
    return success;
    
    
}

// utility routine to display error aleart if takePicture fails
- (void)displayErrorOnMainQueue:(NSError *)error withMessage:(NSString *)message
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ (%d)", message, (int)[error code]]
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    });
}

// main action method to take a still image -- if face detection has been turned on and a face has been detected
// the square overlay will be composited on top of the captured image and saved to the camera roll
BOOL currenttake = false;

- (IBAction)takePicture:(id)sender
{
    [self update_Botton:(id)sender];
}
- (UIImage *)fixOrientation:(UIImage *)aImage {
    
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}
- (void)update_Botton:(id)sender
{
    for (AVCaptureConnection *connection in ((AVCaptureStillImageOutput *) session.outputs[0]).connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if([[port mediaType] isEqual:AVMediaTypeVideo]) {
                videoConnection = connection;
                break;
            }
        }
        if(connection) {
            break;
        }
    }
    [session.outputs[0] captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
        CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
        if (exifAttachments) {
            NSDictionary *dictExif = (__bridge NSDictionary *)exifAttachments;
            for (NSString *key in dictExif) {
                NSLog(@"%@ : %@",key ,[dictExif valueForKey:key]);
            }
        }
        //暫時的功能 可以縮圖顯示的UIImage。
        NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
        showImg.image = [[UIImage alloc] initWithData:imageData];
        //UIImage *temp = [self fixOrientation:showImg.image];
        //UIImageWriteToSavedPhotosAlbum (temp, nil, nil, nil);
    }];
    // Find out the current orientation and tell the still image output.
    AVCaptureConnection *stillImageConnection = [stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    AVCaptureVideoOrientation avcaptureOrientation = [self avOrientationForDeviceOrientation:curDeviceOrientation];
    [stillImageConnection setVideoOrientation:avcaptureOrientation];
    [stillImageConnection setVideoScaleAndCropFactor:effectiveScale];
    
    BOOL doingFaceDetection = detectFaces && (effectiveScale == 1.0);
    
    // set the appropriate pixel format / image type output setting depending on if we'll need an uncompressed image for
    // the possiblity of drawing the red square over top or if we're just writing a jpeg to the camera roll which is the trival case
    if (doingFaceDetection)
        [stillImageOutput setOutputSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCMPixelFormat_32BGRA]
                                                                        forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    else
        [stillImageOutput setOutputSettings:[NSDictionary dictionaryWithObject:AVVideoCodecJPEG
                                                                        forKey:AVVideoCodecKey]];
    
    [stillImageOutput captureStillImageAsynchronouslyFromConnection:stillImageConnection
        completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            if (error) {
              [self displayErrorOnMainQueue:error withMessage:@"Take picture failed"];
            }
            else {
                if (doingFaceDetection) {
                    // Got an image.
                    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(imageDataSampleBuffer);
                    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, imageDataSampleBuffer, kCMAttachmentMode_ShouldPropagate);
                    CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(NSDictionary *)attachments];
                    if (attachments)
                        CFRelease(attachments);
                    
                    NSDictionary *imageOptions = nil;
                    NSNumber *orientation = CMGetAttachment(imageDataSampleBuffer, kCGImagePropertyOrientation, NULL);
                    if (orientation) {
                        imageOptions = [NSDictionary dictionaryWithObject:orientation forKey:CIDetectorImageOrientation];
                    }
                    
                    // when processing an existing frame we want any new frames to be automatically dropped
                    // queueing this block to execute on the videoDataOutputQueue serial queue ensures this
                    // see the header doc for setSampleBufferDelegate:queue: for more information
                    dispatch_sync(videoDataOutputQueue, ^(void) {
                        
                        // get the array of CIFeature instances in the given image with a orientation passed in
                        // the detection will be done based on the orientation but the coordinates in the returned features will
                        // still be based on those of the image.
                        NSArray *features = [faceDetector featuresInImage:ciImage options:imageOptions];
                        CGImageRef srcImage = NULL;
//                        OSStatus err = CreateCGImageFromCVPixelBuffer(CMSampleBufferGetImageBuffer(imageDataSampleBuffer), &srcImage);
//                        check(!err);
                        
                        CGImageRef cgImageResult = [self newSquareOverlayedImageForFeatures:features
                                                                                  inCGImage:srcImage
                                                                            withOrientation:curDeviceOrientation
                                                                                frontFacing:isUsingFrontFacingCamera];
                        if (srcImage)
                            CFRelease(srcImage);
                        
                        CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault,
                                                                                    imageDataSampleBuffer,
                                                                                    kCMAttachmentMode_ShouldPropagate);
                        [self writeCGImageToCameraRoll:cgImageResult withMetadata:(id)attachments];
                        if (attachments)
                            CFRelease(attachments);
                        if (cgImageResult)
                            CFRelease(cgImageResult);
                        
                    });
                    
                    [ciImage release];
                }
                else {
                    // trivial simple JPEG case
                    NSData *jpegData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, 
                                                                                imageDataSampleBuffer, 
                                                                                kCMAttachmentMode_ShouldPropagate);
                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                    [library writeImageDataToSavedPhotosAlbum:jpegData metadata:(id)attachments completionBlock:^(NSURL *assetURL, NSError *error) {
                        if (error) {
                            [self displayErrorOnMainQueue:error withMessage:@"Save to camera roll failed"];
                        }
                    }];
                    
                    if (attachments)
                        CFRelease(attachments);
                    [library release];
                }
            }
        }
     ];
}




- (void)update:(id)sender
{
    // Find out the current orientation and tell the still image output.
    if(currentstate == STATE_SHOTING || currenttake) {
        currentstate = STATE_INIT; //回虧初始狀態;
        
        faceCount = 0; //歸零
        //停止倒數
        [timerPicture invalidate];
        timerPicture = nil;
        [countDown invalidate];
        countDown = nil;
        faceCountLabel.hidden = true;
        labelint = 2;
        [onoffTorch invalidate];
        onoffTorch = nil;
        flash = 6;
        
        for (AVCaptureConnection *connection in ((AVCaptureStillImageOutput *) session.outputs[0]).connections) {
            for (AVCaptureInputPort *port in [connection inputPorts]) {
                if([[port mediaType] isEqual:AVMediaTypeVideo]) {
                    videoConnection = connection;
                    break;
                }
            }
            if(connection) {
                break;
            }
        }
        
        [session.outputs[0] captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
            CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
            if (exifAttachments) {
                NSDictionary *dictExif = (__bridge NSDictionary *)exifAttachments;
                for (NSString *key in dictExif) {
                    NSLog(@"%@ : %@",key ,[dictExif valueForKey:key]);
                }
            }
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
            //      在  的UIImage
            showImg.image = [[UIImage alloc] initWithData:imageData];
            UIImage *temp = [self fixOrientation:showImg.image];
            UIImageWriteToSavedPhotosAlbum (temp, nil, nil, nil);
        }];
    }
    else if(currentstate == STATE_SHOTING_EXIT_BORDER) {
        [timerPicture invalidate];
        timerPicture = nil;
        NSLog(@"Stop SHOOTING  and  currentSTATE : %d",currentstate);
        faceCount = 0; //歸零 才可以再次進行拍照
    }
    //關閉閃光燈
    AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    [flashDevice lockForConfiguration:nil];
    [flashDevice setTorchMode:AVCaptureTorchModeOff];
    [flashDevice unlockForConfiguration];
    
    currenttake = false;
    
}

- (NSNumber *)getImageOrientationByDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
    int exifOrientation;
    
    /* kCGImagePropertyOrientation values
     The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
     by the TIFF and EXIF specifications -- see enumeration of integer constants.
     The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
     
     used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
     If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
    
    enum {
        PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
        PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
        PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
        PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
    };
    
    switch (deviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            if (isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            if (isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
        default:
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            break;
    }
    return [NSNumber numberWithInt:exifOrientation];
}



// turn on/off face detection
- (IBAction)toggleFaceDetection:(id)sender
{
    detectFaces = [(UISwitch *)sender isOn];
    [[videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:detectFaces];
    if (!detectFaces) {
        Capture.enabled = true;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            // clear out any squares currently displaying.
            [self drawFaceBoxesForFeatures:[NSArray array] forVideoBox:CGRectZero orientation:UIDeviceOrientationPortrait];
        });
    }else {
        Capture.enabled = false;
    }
}

// find where the video box is positioned within the preview layer based on the video size and gravity
+ (CGRect)videoPreviewBoxForGravity:(NSString *)gravity frameSize:(CGSize)frameSize apertureSize:(CGSize)apertureSize
{
    CGFloat apertureRatio = apertureSize.height / apertureSize.width;
    CGFloat viewRatio = frameSize.width / frameSize.height;
    
    CGSize size = CGSizeZero;
    if ([gravity isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
        if (viewRatio > apertureRatio) {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        } else {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        }
    } else if ([gravity isEqualToString:AVLayerVideoGravityResizeAspect]) {
        if (viewRatio > apertureRatio) {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        } else {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        }
    } else if ([gravity isEqualToString:AVLayerVideoGravityResize]) {
        size.width = frameSize.width;
        size.height = frameSize.height;
    }
    
    CGRect videoBox;
    videoBox.size = size;
    if (size.width < frameSize.width)
        videoBox.origin.x = (frameSize.width - size.width) / 2;
    else
        videoBox.origin.x = (size.width - frameSize.width) / 2;
    
    if ( size.height < frameSize.height )
        videoBox.origin.y = (frameSize.height - size.height) / 2;
    else
        videoBox.origin.y = (size.height - frameSize.height) / 2;
    
    return videoBox;
}

// called asynchronously as the capture output is capturing sample buffers, this method asks the face detector (if on)
// to detect features and for each draw the red square in a layer and set appropriate orientation
bool stopServo = true;
int face180 = 105;   //定義馬達中間值，開頭define已經定義face360 也就是 Servo360
- (void)drawFaceBoxesForFeatures:(NSArray *)features forVideoBox:(CGRect)clap orientation:(UIDeviceOrientation)orientation
{
    int allfaceY = 0,allfaceX = 0,x=0;
    NSArray *sublayers = [NSArray arrayWithArray:[previewLayer sublayers]];
    NSInteger sublayersCount = [sublayers count], currentSublayer = 0; //總共人臉
    NSInteger featuresCount = [features count], currentFeature = 0;  //目前人臉
    x = (int)featuresCount;     //目前人臉
    NSLog(@"Running drawface !");
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    // hide all the face layers
    for ( CALayer *layer in sublayers ) {
        if ( [[layer name] isEqualToString:@"FaceLayer"] )
            [layer setHidden:YES];
    }
    
    if ( featuresCount == 0 || !detectFaces ) {
        [CATransaction commit];
        _facePointView.hidden = YES;   //當沒臉就隱藏紅點
        
        if(currentstate == STATE_SHOTING) {
            currentstate = STATE_INIT;
            [timerPicture invalidate];
            [countDown invalidate];
            faceCountLabel.hidden = true;
            faceCount = 0;
            labelint = 2;
            [onoffTorch invalidate];
            flash = 6;
        }
        
        //當沒臉就停止馬達轉動
        uint8_t pin = 0x04;
        uint8_t value = Servo360;
        [protocol servoWrite360:pin Value:value];
        NSLog(@"停止轉動");
        
        return; // early bail.
    }
    
    if (stopServo) {
        //停止馬達轉動
        uint8_t pin = 0x04;
        uint8_t value = Servo360;
        [protocol servoWrite360:pin Value:value];
        NSLog(@"停止轉動");
    }
    
    CGSize parentFrameSize = [previewView frame].size;
    NSString *gravity = [previewLayer videoGravity];
    //BOOL isVideoMirrored = [previewLayer isVideoMirrored];
    BOOL isMirrored = [previewLayer isMirrored];
    CGRect previewBox = [FaceTrackingViewController videoPreviewBoxForGravity:gravity
                                                        frameSize:parentFrameSize
                                                     apertureSize:clap.size];
    
    for ( CIFaceFeature *ff in features ) {
        // find the correct position for the square layer within the previewLayer
        // the feature box originates in the bottom left of the video frame.
        // (Bottom right if mirroring is turned on)
        CGRect faceRect = [ff bounds];
        
        // flip preview width and height
        CGFloat temp = faceRect.size.width;
        faceRect.size.width = faceRect.size.height;
        faceRect.size.height = temp;
        temp = faceRect.origin.x;
        faceRect.origin.x = faceRect.origin.y;
        faceRect.origin.y = temp;
        // scale coordinates so they fit in the preview box, which may be scaled
        CGFloat widthScaleBy = previewBox.size.width / clap.size.height;
        CGFloat heightScaleBy = previewBox.size.height / clap.size.width;
        faceRect.size.width *= widthScaleBy;
        faceRect.size.height *= heightScaleBy;
        faceRect.origin.x *= widthScaleBy;
        faceRect.origin.y *= heightScaleBy;
        
        
        //有偵測到臉時 開始畫框以及動作
#pragma mark:frontCam
        if ( isMirrored ) {
            
            faceRect = CGRectOffset(faceRect, previewBox.origin.x + previewBox.size.width - faceRect.size.width - (faceRect.origin.x * 2), previewBox.origin.y);
            for (int i=0 ; i<x ;i++)
            {
                
                allfaceY += (int)faceRect.origin.y + ((int)faceRect.size.width/2) ;
            }
            for (int i=0 ; i<x ;i++)
            {
                allfaceX += (int)faceRect.origin.x + ((int)faceRect.size.width/2) ;
            }
            //            NSLog(@"Smile check : %@ ",ff.hasSmile ? @"Yes" : @"No");
            //            NSLog(@"LeftEyePostion : %@ ",ff.hasLeftEyePosition ? @"YES" : @"NO");
            //            NSLog(@"RightEyePostion : %@ ",ff.hasRightEyePosition ? @"YES" : @"NO");
            //            NSLog(@"LeftEyeClose: %@ ",ff.leftEyeClosed ? @"YES" : @"NO");
            //            NSLog(@"RightEyeClose : %@ ",ff.rightEyeClosed ? @"YES" : @"NO");
            
            _facePointView.hidden = NO;  //打開人臉中心點 紅點
            //_facePointView.frame = CGRectMake(allfaceX/(int)featuresCount/(int)featuresCount, allfaceY/(int)featuresCount/(int)featuresCount+60, 10, 10);
            _facePointView.frame = CGRectMake(allfaceX/x/x, allfaceY/x/x, 10, 10);
            NSLog(@"人臉中心 Y  座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
            NSLog(@"人臉中心 X  座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
            NSLog(@"featuresCount 數量 : %d",x);
            NSLog(@"allface / featureCount = %d" , (allfaceY/x/x));  //全部人臉y值加總後除以全部人臉數目 = 多人中心點
            NSLog(@"currentSublayer 最後人臉數量: %d",(int)currentSublayer);
            
            // 人臉框中心點 小於 手動調整範圍框的左邊  馬達往左轉   判斷人臉到齊才可以轉臉 或者一張臉也可以轉
            if(((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.y &&
                (int)featuresCount==1 ) || ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.y && x==(int)currentSublayer))
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                    }
                }
                uint8_t pin = 0x04;
                uint8_t value = Servo358;
                [protocol servoWrite360:pin Value:value];
                NSLog(@"人臉偏左");
                NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
                NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
                NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
                stopServo = false;
            }
            
            // 人臉框中心點 大於 手動調整範圍框的右邊  馬達往右轉  判斷人臉到齊才可以轉臉 或者一張臉也可以轉
            else if(((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.size.height+(int)_border.frame.origin.y && (int)featuresCount==1 ) || ((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.size.height+(int)_border.frame.origin.y && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                    }
                }
                uint8_t pin = 0x04;
                uint8_t value = Servo362;
                [protocol servoWrite360:pin Value:value];
                NSLog(@"人臉偏右");
                NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
                NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
                NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
                stopServo = false;
            }
            
            //人臉偏上，180度馬達轉上
            if(((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x+(int)_border.frame.size.width && (int)featuresCount==1 ) || ((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x+(int)_border.frame.size.width && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                    }
                }
                if (face180 <= maxlimit) {
                    uint8_t pin = 0x03;
                    uint8_t value = face180 += 1;
                    [protocol servoWrite180:pin Value:value];
                    NSLog(@"人臉偏上");
                    NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
                    
                    NSLog(@"Border top : %d",(int)_border.frame.origin.x+(int)_border.frame.size.width);
                    NSLog(@"Border bottom : %d",(int)_border.frame.origin.x);
                    stopServo = true;
                    
                }
                else if ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y
                         && (allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y){
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"人臉偏左或右 停止360馬達轉動");
                }
            }
            
            //人臉偏下，180度馬達轉下
            else if(((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x && (int)featuresCount==1 ) || ((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                    }
                }
                if (face180 >= 80) {
                    uint8_t pin = 0x03;
                    uint8_t value = face180 -= 1;
                    [protocol servoWrite180:pin Value:value];
                    NSLog(@"人臉偏下");
                    NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
                    
                    NSLog(@"Border top : %d",(int)_border.frame.origin.x+(int)_border.frame.size.width);
                    NSLog(@"Border bottom : %d",(int)_border.frame.origin.x);
                    stopServo = true;
                }
                else if ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y
                         && (allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y){
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"人臉偏左或右 停止360馬達轉動");
                }
            }
            if (
                //大於注視框 左邊
                ((((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y && (int)featuresCount==1 ) ||
                  ((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y && x==(int)currentSublayer))
                 &&
                 //小於注視框 右邊
                 (((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y && (int)featuresCount==1 ) ||
                  ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y && x==(int)currentSublayer)))
                &&
                //小於注視框 上方
                ((((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x+(int)_border.frame.size.width && (int)featuresCount==1 ) ||
                  ((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x+(int)_border.frame.size.width && x==(int)currentSublayer))
                 &&
                 //大於注視框 下方
                 (((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x && (int)featuresCount==1 ) ||
                  ((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x && x==(int)currentSublayer)))
                )
            {
                if(currentstate == STATE_INIT){
                    currentstate = STATE_INSIDE_BORDER;
                    NSLog(@"INIT currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_INSIDE_BORDER) {
                    //如果在視訊框內偵測到微笑及轉換微笑模式等待扎眼
                    if(ff.hasSmile)currentstate = STATE_SMILE;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"INSIDE_BORDER currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_SMILE) {
                    //偵測到左右眼閉眼 即進入 拍照模式
                    if(ff.leftEyeClosed && ff.rightEyeClosed)currentstate = STATE_SHOTING;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"SMILE currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_SHOTING_EXIT_BORDER) {
                    currentstate = STATE_SHOTING;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"SMILE currentSTATE : %d ",currentstate);
                }
            }
            
            NSLog(@"labelint = %d",labelint);
            NSLog(@"目前狀態 currentState : %d and faceCount : %d",currentstate,faceCount);
            if(currentstate == STATE_SHOTING && faceCount == 0){  //拍照倒數
                timerPicture = [NSTimer scheduledTimerWithTimeInterval:3 // 秒
                                                                target:self
                                                              selector:@selector(update:) //倒數三秒拍照
                                                              userInfo:nil
                                                               repeats:NO];
                countDown = [NSTimer scheduledTimerWithTimeInterval:1 // 秒
                                                             target:self
                                                           selector:@selector(showlabel:) //倒數Label提示
                                                           userInfo:nil
                                                            repeats:YES];
                faceCount++;
            }
            
            
        }
#pragma mark:backCam
        // 後鏡頭模式
        else  {
            faceRect = CGRectOffset(faceRect, previewBox.origin.x, previewBox.origin.y);
            for (int i=0 ; i<x ;i++)
            {
                allfaceY += (int)faceRect.origin.y + ((int)faceRect.size.width/2) ;
            }
            for (int i=0 ; i<x ;i++)
            {
                allfaceX += (int)faceRect.origin.x + ((int)faceRect.size.width/2) ;
            }
            //            NSLog(@"Smile check : %@ ",ff.hasSmile ? @"Yes" : @"No");
            //            NSLog(@"LeftEyePostion : %@ ",ff.hasLeftEyePosition ? @"YES" : @"NO");
            //            NSLog(@"RightEyePostion : %@ ",ff.hasRightEyePosition ? @"YES" : @"NO");
            //            NSLog(@"LeftEyeClose: %@ ",ff.leftEyeClosed ? @"YES" : @"NO");
            //            NSLog(@"RightEyeClose : %@ ",ff.rightEyeClosed ? @"YES" : @"NO");
            
            _facePointView.hidden = NO;  //打開人臉中心點 紅點
            //_facePointView.frame = CGRectMake(allfaceX/(int)featuresCount/(int)featuresCount, allfaceY/(int)featuresCount/(int)featuresCount+60, 10, 10);
            _facePointView.frame = CGRectMake(allfaceX/x/x, allfaceY/x/x, 10, 10);
            NSLog(@"人臉中心 Y  座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
            NSLog(@"人臉中心 X  座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
            NSLog(@"featuresCount 數量 : %d",x);
            NSLog(@"allface / featureCount = %d" , (allfaceY/x/x));  //全部人臉y值加總後除以全部人臉數目 = 多人中心點
            NSLog(@"currentSublayer 最後人臉數量: %d",(int)currentSublayer);
            
            // 人臉框中心點 小於 手動調整範圍框的左邊  馬達往左轉   判斷人臉到齊才可以轉臉 或者一張臉也可以轉
            if(((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.y && (int)featuresCount==1 ) || ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.y&& x==(int)currentSublayer))
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                        [onoffTorch invalidate];
                        flash = 6;
                        //出框關閉閃光燈
                        AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                        [flashDevice lockForConfiguration:nil];
                        [flashDevice setTorchMode:AVCaptureTorchModeOff];
                        [flashDevice unlockForConfiguration];
                        
                    }
                }
                uint8_t pin = 0x04;
                uint8_t value = Servo362;
                [protocol servoWrite360:pin Value:value];
                NSLog(@"人臉偏右");
                NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
                
                NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
                NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
                stopServo = false;
            }
            // 人臉框中心點 大於 手動調整範圍框的右邊  馬達往右轉  判斷人臉到齊才可以轉臉 或者一張臉也可以轉
            else if(((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.size.height+(int)_border.frame.origin.y && (int)featuresCount==1 ) || ((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.size.height+(int)_border.frame.origin.y && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                        [onoffTorch invalidate];
                        flash = 6;
                        //出框關閉閃光燈
                        AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                        [flashDevice lockForConfiguration:nil];
                        [flashDevice setTorchMode:AVCaptureTorchModeOff];
                        [flashDevice unlockForConfiguration];
                    }
                }
                uint8_t pin = 0x04;
                uint8_t value = Servo358;
                [protocol servoWrite360:pin Value:value];
                NSLog(@"人臉偏右");
                NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.y + ((int)faceRect.size.width/2));
                
                NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
                NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
                stopServo = false;
            }
            if(((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x+(int)_border.frame.size.width && (int)featuresCount==1 ) || ((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x+(int)_border.frame.size.width && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                        [onoffTorch invalidate];
                        flash = 6;
                        //出框關閉閃光燈
                        AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                        [flashDevice lockForConfiguration:nil];
                        [flashDevice setTorchMode:AVCaptureTorchModeOff];
                        [flashDevice unlockForConfiguration];
                    }
                }
                if (face180 <= maxlimit) {
                    uint8_t pin = 0x03;
                    uint8_t value = face180 += 1;
                    [protocol servoWrite180:pin Value:value];
                    NSLog(@"人臉偏上");
                    NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
                    
                    NSLog(@"Border top : %d",(int)_border.frame.origin.x+(int)_border.frame.size.width);
                    NSLog(@"Border bottom : %d",(int)_border.frame.origin.x);
                    stopServo = true;
                }
                else if ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y
                         && (allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y){
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"人臉偏左或右 停止360馬達轉動");
                }
            }
            else if(((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x && (int)featuresCount==1 ) || ((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x && x==(int)currentSublayer) )
            {
                if(currentstate == STATE_SHOTING) {
                    currentstate = STATE_SHOTING_EXIT_BORDER;
                    faceCount = 0;
                    if (timerPicture.isValid){
                        [timerPicture invalidate];
                        [countDown invalidate];
                        faceCountLabel.hidden = true;
                        labelint = 2;
                        [onoffTorch invalidate];
                        flash = 6;
                        //出框關閉閃光燈
                        AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
                        [flashDevice lockForConfiguration:nil];
                        [flashDevice setTorchMode:AVCaptureTorchModeOff];
                        [flashDevice unlockForConfiguration];
                    }
                }
                if (face180 >= 80) {
                    uint8_t pin = 0x03;
                    uint8_t value = face180 -= 1;
                    [protocol servoWrite180:pin Value:value];
                    NSLog(@"人臉偏下");
                    NSLog(@"人臉中心座標  :  %d ", (int)faceRect.origin.x + ((int)faceRect.size.width/2));
                    
                    NSLog(@"Border top : %d",(int)_border.frame.origin.x+(int)_border.frame.size.width);
                    NSLog(@"Border bottom : %d",(int)_border.frame.origin.x);
                    stopServo = true;
                }
                else if ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y
                         && (allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y){
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"人臉偏左或右 停止360馬達轉動");
                }
            }
            if (
                //大於注視框 左邊
                ((((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y && (int)featuresCount==1 ) ||
                  ((allfaceY/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.y && x==(int)currentSublayer))
                 &&
                 //小於注視框 右邊
                 (((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y && (int)featuresCount==1 ) ||
                  ((allfaceY/(int)featuresCount)/(int)featuresCount < (int)_border.frame.size.height+(int)_border.frame.origin.y && x==(int)currentSublayer)))
                &&
                //小於注視框 上方
                ((((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x+(int)_border.frame.size.width && (int)featuresCount==1 ) ||
                  ((allfaceX/(int)featuresCount)/(int)featuresCount < (int)_border.frame.origin.x+(int)_border.frame.size.width && x==(int)currentSublayer))
                 &&
                 //大於注視框 下方
                 (((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x && (int)featuresCount==1 ) ||
                  ((allfaceX/(int)featuresCount)/(int)featuresCount > (int)_border.frame.origin.x && x==(int)currentSublayer)))
                )
            {
                if(currentstate == STATE_INIT){
                    currentstate = STATE_INSIDE_BORDER;
                    NSLog(@"INIT currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_INSIDE_BORDER) {
                    //如果在視訊框內偵測到微笑及轉換微笑模式等待扎眼
                    if(ff.hasSmile)currentstate = STATE_SMILE;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"INSIDE_BORDER currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_SMILE) {
                    //偵測到左右眼閉眼 即進入 拍照模式
                    if(ff.leftEyeClosed && ff.rightEyeClosed)currentstate = STATE_SHOTING;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"SMILE currentSTATE : %d ",currentstate);
                }
                else if(currentstate == STATE_SHOTING_EXIT_BORDER) {
                    currentstate = STATE_SHOTING;
                    stopServo = true;
                    uint8_t pin = 0x04;
                    uint8_t value = Servo360;
                    [protocol servoWrite360:pin Value:value];
                    NSLog(@"有人臉框 安全範圍 停止轉動");
                    NSLog(@"SMILE currentSTATE : %d ",currentstate);
                }
            }
            
            NSLog(@"目前狀態 currentState : %d and faceCount : %d",currentstate,faceCount);
            //後鏡頭
            if(currentstate == STATE_SHOTING && faceCount == 0){
                timerPicture = [NSTimer scheduledTimerWithTimeInterval:3 // 秒
                                                                target:self
                                                              selector:@selector(update:)
                                                              userInfo:nil
                                                               repeats:NO];
                countDown = [NSTimer scheduledTimerWithTimeInterval:1 // 秒
                                                             target:self
                                                           selector:@selector(showlabel:)
                                                           userInfo:nil
                                                            repeats:YES];
                onoffTorch = [NSTimer scheduledTimerWithTimeInterval:0.5 // 秒
                                                              target:self
                                                            selector:@selector(torchFlash:)
                                                            userInfo:nil
                                                             repeats:YES];
                
                faceCount++;
            }
        }
        
        CALayer *featureLayer = nil;
        
        // re-use an existing layer if possible
        while ( !featureLayer && (currentSublayer < sublayersCount) ) {
            CALayer *currentLayer = [sublayers objectAtIndex:currentSublayer++];
            if ( [[currentLayer name] isEqualToString:@"FaceLayer"] ) {
                featureLayer = currentLayer;
                [currentLayer setHidden:NO];
            }
        }
        
        // create a new one if necessary
        if ( !featureLayer ) {
            featureLayer = [CALayer new];
            [featureLayer setContents:(id)[faceBox CGImage]];
            //featureLayer setContents:(id)[square CGImage]];
            [featureLayer setName:@"FaceLayer"];
            [previewLayer addSublayer:featureLayer];
            [featureLayer release];
        }
        
        if (ff.hasSmile) {
            [featureLayer setFrame:faceRect];
        }
        else {
            [featureLayer setHidden:YES];
            //[featureLayer release];  //while on it, app will hang.
        }
        
        switch (orientation) {
            case UIDeviceOrientationPortrait:
                [featureLayer setAffineTransform:CGAffineTransformMakeRotation(DegreesToRadians(0.))];
                break;
            case UIDeviceOrientationPortraitUpsideDown:
                [featureLayer setAffineTransform:CGAffineTransformMakeRotation(DegreesToRadians(180.))];
                break;
            case UIDeviceOrientationLandscapeLeft:
                [featureLayer setAffineTransform:CGAffineTransformMakeRotation(DegreesToRadians(90.))];
                break;
            case UIDeviceOrientationLandscapeRight:
                [featureLayer setAffineTransform:CGAffineTransformMakeRotation(DegreesToRadians(-90.))];
                break;
            case UIDeviceOrientationFaceUp:
            case UIDeviceOrientationFaceDown:
            default:
                break; // leave the layer in its last known orientation
        }
        currentFeature++;
    }
    //計算多人臉時啟動
    if((int)currentSublayer >  (int)currentFeature) currentSublayer = 0;
    [CATransaction commit];
    
}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    NSLog(@"Run captureOutputing !!!!");
    //[NSThread sleepForTimeInterval:1];
    // got an image
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(NSDictionary *)attachments];
    if (attachments)
        CFRelease(attachments);
    NSDictionary *imageOptions = nil;
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    int exifOrientation;
    
    /* kCGImagePropertyOrientation values
     The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
     by the TIFF and EXIF specifications -- see enumeration of integer constants.
     The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
     
     used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
     If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
    
    enum {
        PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
        PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
        PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
        PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
    };
    
    switch (curDeviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            if (isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            if (isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
        default:
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            break;
    }
    
    
    imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:exifOrientation] forKey:CIDetectorImageOrientation];
    
    
    //NSArray *features = [faceDetector featuresInImage:ciImage options:imageOptions];
    //加入人臉微笑辨識
    NSArray *features = [faceDetector featuresInImage:ciImage options:@{ CIDetectorSmile : @YES,
                                                                         CIDetectorEyeBlink : @YES,
                                                                         CIDetectorImageOrientation :[NSNumber numberWithInt:exifOrientation] }];
    [ciImage release];
    
    // get the clean aperture
    // the clean aperture is a rectangle that defines the portion of the encoded pixel dimensions
    // that represents image data valid for display.
    CMFormatDescriptionRef fdesc = CMSampleBufferGetFormatDescription(sampleBuffer);
    CGRect clap = CMVideoFormatDescriptionGetCleanAperture(fdesc, false /*originIsTopLeft == false*/);
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSLog(@" 開始畫框 ");
        [self drawFaceBoxesForFeatures:features forVideoBox:clap orientation:curDeviceOrientation];
    });
}

- (void)dealloc
{
    [showImg release];
    //[bottomToolbar release];
    [Capture release];
    //[self teardownAVCapture];
    [faceDetector release];
    [square release];
    [_border release];
    [_facePointView release];
    [_handlePinch release];
    [_panBorder release];
    //[super dealloc];  //沒清除 0530
}

// use front/back camera
- (IBAction)switchCameras:(id)sender
{
    AVCaptureDevicePosition desiredPosition;
    if (isUsingFrontFacingCamera)
        desiredPosition = AVCaptureDevicePositionBack;
    else
        desiredPosition = AVCaptureDevicePositionFront;
    
    
    for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
        if ([d position] == desiredPosition) {
            [[previewLayer session] beginConfiguration];
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
            for (AVCaptureInput *oldInput in [[previewLayer session] inputs]) {
                [[previewLayer session] removeInput:oldInput];
            }
            [[previewLayer session] addInput:input];
            [[previewLayer session] commitConfiguration];
            break;
        }
    }
    isUsingFrontFacingCamera = !isUsingFrontFacingCamera;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    //[baseView bringSubviewToFront:topView];
    //[baseView bringSubviewToFront:bottomView];
    [super viewDidLoad];
    [[UIScreen mainScreen] setBrightness:0.5]; //設定螢幕亮度
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES]; //設定閒置不黑屏
    
    flash = 6;  //倒數時給予後鏡頭閃光提示
    
    // Do any additional setup after loading the view, typically from a nib.
    [self setupAVCapture];
    //square = [[UIImage imageNamed:@"squarePNG"] retain];
    
    square = [[UIImage imageNamed:@"clear"] retain];
    NSDictionary *detectorOptions = [[NSDictionary alloc] initWithObjectsAndKeys:CIDetectorAccuracyLow, CIDetectorAccuracy, nil];
    faceDetector = [[CIDetector detectorOfType:CIDetectorTypeFace context:nil options:detectorOptions] retain];
    [detectorOptions release];
    
    faceBox = [[UIImage imageNamed:@"Border"] retain];
    
    UIImage *temp = [[UIImage imageNamed:@"jc.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    //temp = self.myImg.image ;
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithImage:temp style:UIBarButtonItemStyleBordered target:self action:@selector(action)];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    self.navigationItem.leftBarButtonItem.enabled = NO;
    //[self.view bringSubviewToFront:(UIImageView *)_myImg];
    [self.view bringSubviewToFront:(UIToolbar *)bottomToolbar];
    
    //新增動態注視框並且加入到mainview裡
    CGRect frameRect = CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height/2 - (bottomToolbar.frame.size.height), 100, 120);
    _border = [[UIImageView alloc] initWithFrame:frameRect];
    _border.userInteractionEnabled = YES;
    _border.image = [UIImage imageNamed:@"border_1.png"];
    [self.view addSubview:_border];  //加入動態視窗到view ;
    [_border release];
    
    //[self.view bringSubviewToFront:_border]; //放到視窗最上面;
    
    // 新增pinch動作 並且加入到border圖片裡面
    UIPinchGestureRecognizer *handlePinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imagePinch:)];
    handlePinch.delegate = self;
    [self.border addGestureRecognizer:handlePinch];
    [handlePinch release];
    
    
    // 新增Pan手勢動作，並且加入到border圖片裡面使用
    UIPanGestureRecognizer *panBorder = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(imagePan:)];
    //panBorder.maximumNumberOfTouches = 2;
    [panBorder setMinimumNumberOfTouches:1]; //設定最少手指觸發
    [panBorder setMaximumNumberOfTouches:1]; //設定最多手指觸發
    panBorder.delegate = self;
    [self.border addGestureRecognizer:panBorder];
    [panBorder release];
    
    //加入 Point 提醒人臉中心點為哪裡以及 多人臉中心點落在哪裡
    frameRect = CGRectMake(0, 0, 10, 10);
    //CGRect facePoint = _facePointView.frame;
    _facePointView = [[UIView alloc] initWithFrame:frameRect];
    _facePointView.backgroundColor = [UIColor redColor];
    _facePointView.hidden = YES; //先把紅點隱藏起來
    [self.view addSubview:_facePointView];
    [_facePointView release];
    
    //新增倒數提示Label
    frameRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height/2, 300, 300);
    faceCountLabel = [[UILabel alloc] initWithFrame:frameRect];
    faceCountLabel.center = CGPointMake(self.view.bounds.size.width / 2 , self.view.bounds.size.height / 2); //設定字顯示在中間
    [faceCountLabel setFont:[UIFont fontWithName:@"Helvetica" size:250]];
    [faceCountLabel setNumberOfLines:0];
    faceCountLabel.textAlignment = UITextAlignmentCenter; //置中
    faceCountLabel.alpha=0.8;
    [faceCountLabel setTextColor:[UIColor whiteColor]];   //字體顏色
    faceCountLabel.hidden = true;
    faceCountLabel.transform = CGAffineTransformMakeRotation(M_PI/2);  //字體旋轉90度
    //faceCountLabel.text = [NSString stringWithFormat:@"%d", i];
    [self.view addSubview:faceCountLabel];
    [faceCountLabel release];
    
    
    
    
    
    protocol = [[RBLProtocol alloc] init];
    protocol.delegate = self;
    protocol.ble = ble;
}

//  新增放大縮小
- (void)imagePinch:(UIPinchGestureRecognizer *)recognizer
{
    
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
    //NSLog(@"%f",_border.bounds.size);
    NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
    NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
}

//  新增拖曳border
-(void)imagePan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x+translation.x, recognizer.view.center.y+translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    NSLog(@"Border 左邊 : %d",(int)_border.frame.origin.y);
    NSLog(@"Border 右邊 : %d",(int)_border.frame.origin.y+(int)_border.frame.size.height);
    
}

int labelint=2;
-(void)showlabel:(id)sender
{
    NSLog(@"Running function showlabel..... ");
    if(currentstate == STATE_SHOTING){
        faceCountLabel.hidden = false;
        faceCountLabel.text = [NSString stringWithFormat:@"%d", labelint];
        labelint--;
        if(labelint == -1){
            faceCountLabel.hidden = true;
            labelint = 2;
            [countDown invalidate];
            countDown = nil;
        }
    }
    else{
        faceCountLabel.hidden = true;
        labelint = 2;
        [countDown invalidate];
        countDown = nil;
    }
}
//flash = 6;  //倒數時給予後鏡頭閃光提示
float torchlevel = 0.3; //閃光燈亮度
-(void)torchFlash:(id)sender
{
    AVCaptureDevice *flashDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if(flashDevice.hasTorch && flash%2==0) {
        [flashDevice lockForConfiguration:nil];
        [flashDevice setTorchModeOnWithLevel:torchlevel   error:nil];
        //[flashDevice setTorchMode:AVCaptureTorchModeOn];
        [flashDevice unlockForConfiguration];
    }
    else {
        [flashDevice lockForConfiguration:nil];
        [flashDevice setTorchMode:AVCaptureTorchModeOff];
        [flashDevice unlockForConfiguration];
    }
    flash--;
    if(flash == 0) flash = 6;
}

- (IBAction)btnStopClicked:(id)sender
{
    [session stopRunning];
    [session release];
    [[ble CM] cancelPeripheralConnection:[ble activePeripheral]];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //隱藏 top bar
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ( [gestureRecognizer isKindOfClass:[UIPinchGestureRecognizer class]] ) {
        beginGestureScale = effectiveScale;
    }
    return YES;
}

-(void) bleDidConnect
{
    
    // send reset
    UInt8 buf[] = {0x04, 0x00, 0x00};
    NSData *data = [[NSData alloc] initWithBytes:buf length:3];
    [ble write:data];
    
}

-(BOOL)prefersStatusBarHidden
{
    //關閉status bar
    return YES;
}

@end
