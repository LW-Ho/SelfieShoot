//
//  RBLProtocol.h
//  SimpleControls
//
//  Created by ChenYu Ho on 3/9/16.
//  Copyright © 2016 RedBearLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLE.h"

// Pin modes.
// except from UNAVAILABLE taken from Firmata.h
#define SERVO                   0x04

@protocol ProtocolDelegate
@optional
@required
-(void) protocolDidReceiveCustomData:(uint8_t *) data length:(uint8_t) length;
@end
@interface RBLProtocol : NSObject

@property (strong, nonatomic) BLE *ble;
@property (nonatomic,assign) id <ProtocolDelegate> delegate;
//-(void) parseData:(unsigned char *) data length:(int) length;

-(void) servoWrite180:(uint8_t) pin Value:(uint8_t) value; /* write servo pin, value = angle */
-(void) servoWrite360:(uint8_t) pin Value:(uint8_t) value; /* write servo pin, value = angle */

//-(void) servoRead:(uint8_t) pin;

@end
