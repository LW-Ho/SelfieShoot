//
//  RBLProtocol.m
//  SimpleControls
//
//  Created by ChenYu Ho on 3/9/16.
//  Copyright © 2016 RedBearLab. All rights reserved.
//

#import "RBLProtocol.h"

@implementation RBLProtocol
@synthesize ble;




-(void) servoWrite180:(uint8_t) pin Value:(uint8_t) value
{
#if defined(PROT_DEBUG)
    NSLog(@"RBLPRotocol: servoWrite180");
#endif
    
    //UInt8 buf[3] = {0x03, 0x00, 0x00};
    uint8_t buf[3] = {0x03, 0x00, 0x00};
    buf[1] = value;
    uint8_t len = 3;
    NSData *nsData = [[NSData alloc] initWithBytes:buf length:len];
    [ble write:nsData];
    NSLog(@"%@",nsData);
}

-(void) servoWrite360:(uint8_t)pin Value:(uint8_t)value
{
#if defined(PROT_DEBUG)
    NSLog(@"RBLPRotocol: servoWrite360");
#endif
    
    //UInt8 buf[3] = {0x03, 0x00, 0x00};
    uint8_t buf[3] = {0x04, 0x00, 0x00};
    buf[1] = value;
    uint8_t len = 3;
    NSData *nsData = [[NSData alloc] initWithBytes:buf length:len];
    [ble write:nsData];
    NSLog(@"%@",nsData);
}


@end
