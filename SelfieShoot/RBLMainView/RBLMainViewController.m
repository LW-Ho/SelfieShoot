//
//  RBLMainViewController.m
//  JCshot
//
//  Created by ChenYu Ho on 4/11/16.
//  Copyright © 2016 ChenYu Ho. All rights reserved.
//

#import "RBLMainViewController.h"
#import "FaceTrackingViewController.h"
// 測試小PCB
//#define BlendMicro FALSE
//
//#if BlendMicro
//#   define Servo360 106
//#else
//#   define Servo360 90
//#endif

NSString * const  UUIDPrefKey = @"UUIDPrefKey";

@implementation RBLMainViewController
@synthesize ble;
@synthesize protocol;
FaceTrackingViewController *cv;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[UIScreen mainScreen] setBrightness:0.4]; //設定螢幕亮度
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES]; //設定閒置不黑屏
    
    ble = [[BLE alloc] init];
    [ble controlSetup];
    ble.delegate = self;
    
    self.mDevices = [[NSMutableArray alloc] init];
    self.mDevicesName = [[NSMutableArray alloc] init];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:UUIDPrefKey];
    }
    
    //Retrieve saved UUID from system
    self.lastUUID = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDPrefKey];
    if ([self.lastUUID isEqualToString:@""])
    {
        [btnConnectLast setEnabled:NO];
    }
    else
    {
        [btnConnectLast setEnabled:YES];
    }
    protocol = [[RBLProtocol alloc] init];
    protocol.delegate = self;
    protocol.ble = ble;
}

- (void)viewWillAppear:(BOOL)animated
{
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    //[self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDevice"])
    {
        RBLDetailViewController *vc = [segue destinationViewController];
        vc.BLEDevices = self.mDevices;
        vc.BLEDevicesName = self.mDevicesName;
        vc.ble = ble;
    }
    else if ([[segue identifier] isEqualToString:@"gotoControlVC"])
    {
                cv = [segue destinationViewController];
                cv.ble = ble;
    }
}

-(void) connectionTimer:(NSTimer *)timer
{
    showAlert = YES;
    [btnConnect setEnabled:YES];
    
    self.lastUUID = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDPrefKey];
    
    if ([self.lastUUID isEqualToString:@""])
    {
        [btnConnectLast setEnabled:NO];
    }
    else
    {
        [btnConnectLast setEnabled:YES];
    }
    
    if (ble.peripherals.count > 0)
    {
        if(isFindingLast)
        {
            int i;
            for (i = 0; i < ble.peripherals.count; i++)
            {
                CBPeripheral *p = [ble.peripherals objectAtIndex:i];
                
                if (p.identifier.UUIDString != NULL)
                {
                    //Comparing UUIDs and call connectPeripheral is matched
                    if([self.lastUUID isEqualToString:p.identifier.UUIDString])
                    {
                        showAlert = NO;
                        [ble connectPeripheral:p];
                    }
                }
            }
        }
        else
        {
            [self.mDevices removeAllObjects];
            [self.mDevicesName removeAllObjects];
            
            int i;
            for (i = 0; i < ble.peripherals.count; i++)
            {
                CBPeripheral *p = [ble.peripherals objectAtIndex:i];
                
                if (p.identifier.UUIDString != NULL)
                {
                    [self.mDevices insertObject:p.identifier.UUIDString atIndex:i];
                    if (p.name != nil) {
                        [self.mDevicesName insertObject:p.name atIndex:i];
                    } else {
                        [self.mDevicesName insertObject:@"RedBear Device" atIndex:i];
                    }
                }
                else
                {
                    [self.mDevices insertObject:@"NULL" atIndex:i];
                    [self.mDevicesName insertObject:@"RedBear Device" atIndex:i];
                }
            }
            showAlert = NO;
            [self performSegueWithIdentifier:@"showDevice" sender:self];
        }
    }
    
    if (showAlert == YES) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                       message:@"No BLE Device(s) found."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleCancel
                                                              handler:nil];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [activityScanning stopAnimating];
}

- (IBAction)btnConnectClicked:(id)sender
{
    if (ble.activePeripheral)
        if(ble.activePeripheral.state == CBPeripheralStateConnected)
        {
            [[ble CM] cancelPeripheralConnection:[ble activePeripheral]];
            return;
        }
    
    if (ble.peripherals)
        ble.peripherals = nil;
    
    [btnConnect setEnabled:false];
    [btnConnectLast setEnabled:NO];
    [ble findBLEPeripherals:3];
    
    [NSTimer scheduledTimerWithTimeInterval:(float)3.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    
    isFindingLast = false;
    [activityScanning startAnimating];
}

- (IBAction)lastClick:(id)sender {
    if (ble.peripherals) {
        ble.peripherals = nil;
    }
    
    [btnConnect setEnabled:false];
    [btnConnectLast setEnabled:NO];
    [ble findBLEPeripherals:3];
    
    [NSTimer scheduledTimerWithTimeInterval:(float)3.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    
    isFindingLast = true;
    [activityScanning startAnimating];
}
-(void) bleDidConnect
{
    
    NSLog(@"->DidConnect");
    
    self.lastUUID = ble.activePeripheral.identifier.UUIDString;
    [[NSUserDefaults standardUserDefaults] setObject:self.lastUUID forKey:UUIDPrefKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    self.lastUUID = [[NSUserDefaults standardUserDefaults] objectForKey:UUIDPrefKey];
    if ([self.lastUUID isEqualToString:@""]) {
        [btnConnectLast setEnabled:NO];
    } else {
        [btnConnectLast setEnabled:YES];
    }
    
    [activityScanning stopAnimating];
    [self performSegueWithIdentifier:@"gotoControlVC" sender:self];
}

- (void)bleDidDisconnect
{
    NSLog(@"->DidDisconnect");
    
    [activityScanning stopAnimating];
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void) bleDidReceiveData:(unsigned char *)data length:(int)length
{
#if defined(MV_DEBUG)
    NSLog(@"->DidReceiveData");
#endif
    
        if (cv != nil)
        {
            //[cv processData:data length:length];
        }
}

-(void) bleDidUpdateRSSI:(NSNumber *) rssi
{
}

-(NSString *)getUUIDString:(CFUUIDRef)ref {
    NSString *str = [NSString stringWithFormat:@"%@", ref];
    return [[NSString stringWithFormat:@"%@", str] substringWithRange:NSMakeRange(str.length - 36, 36)];
}

@end
