//
//  RBLMainViewController.h
//  JCshot
//
//  Created by ChenYu Ho on 4/11/16.
//  Copyright © 2016 ChenYu Ho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RBLDetailViewController.h"
#import "RBLProtocol.h"
#import "BLE.h"

@interface RBLMainViewController : UIViewController <BLEDelegate,ProtocolDelegate>
{
    IBOutlet UIActivityIndicatorView *activityScanning;
    IBOutlet UIButton *btnConnect;
    IBOutlet UIButton *btnConnectLast;
    BOOL showAlert;
    bool isFindingLast;
}


@property (strong, nonatomic) BLE *ble;
@property (strong, nonatomic) RBLProtocol *protocol;
@property (strong, nonatomic) NSMutableArray *mDevices;
@property (strong, nonatomic) NSMutableArray *mDevicesName;
@property (strong,nonatomic) NSString *lastUUID;

@end
